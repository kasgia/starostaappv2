package com.starostaapp.backend.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@Document(collection = "Exam")
public class Exam {
    @Id
    @Indexed(unique = true)
    private Integer id;

    @NotBlank
    private String subjectname;

    private Teacher teacher;

    @NotBlank
    @Size(max=10)
    private String group;

    @NotBlank
    private String date;

    public Exam(Integer id, @NotBlank String subjectname, Teacher teacher, @NotBlank @Size(max = 10) String group, @NotBlank String date) {
        this.id = id;
        this.subjectname = subjectname;
        this.teacher = teacher;
        this.group = group;
        this.date = date;
    }
}
