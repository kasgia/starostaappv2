package com.starostaapp.backend.model;

import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@ToString
@Document(collection = "Student")
public class Student {

    @Id
    @Indexed(unique = true)
    private Integer id;

    @NotBlank
    @Size(max=20)
    private String name;

    @NotBlank
    @Size(max=20)
    private String surname;

    @NotBlank
    private Boolean leaderOfGroup;

    @NotBlank
    @Size(max=20)
    private String password;

    public Student() { }

    public Student(Integer id, @NotBlank @Size(max = 20) String name, @NotBlank @Size(max = 20) String surname, @NotBlank Boolean leaderOfGroup) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.leaderOfGroup = leaderOfGroup;
        this.password = null;
    }

    public String getPassword() {
        return password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Boolean getLeaderOfGroup() {
        return leaderOfGroup;
    }

    public void setLeaderOfGroup(Boolean leaderOfGroup) {
        this.leaderOfGroup = leaderOfGroup;
    }
}
