package com.starostaapp.backend.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.ArrayList;

@Getter
@Setter
@Document(collection = "Group")
public class Group {
    @Id
    @Indexed(unique = true)
    private Integer id;

    @NotBlank
    @Size(max = 40)
    private String subjectname;

    @NotBlank
    @Size(max = 10)
    @Indexed(unique = true)
    private String nameofgroup;

    @NotEmpty
    private ArrayList<Teacher> teachers;

    @NotEmpty
    private ArrayList<Student> students;

    public Group() { }

    public void addStudent(Student newStudent){
        students.add(newStudent);
    }

    public void deleteStudent(Integer idStudent) {
        for (Student studentToRemove: students) {
            if (studentToRemove.getId().equals(idStudent)) {
                students.remove(studentToRemove);
                break;
            }
        }
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", subjectname='" + subjectname + '\'' +
                ", nameofgroup='" + nameofgroup + '\'' +
                ", teachers=" + teachers +
                ", students=" + students +
                '}';
    }

    public Student findStudent(Integer idStudent) {
        Student student = new Student();
        for (Student studentToFind: students) {
            if (studentToFind.getId().equals(idStudent)) {
                student = studentToFind;
                break;
            }
        }
        return student;
    }

    public Teacher findTeacher(String name, String surname) {
        Teacher teacher = new Teacher();
        for (Teacher teacherToFind: teachers) {
            if (teacherToFind.getName().equals(name) && teacherToFind.getSurname().equals(surname)) {
                teacher = teacherToFind;
                break;
            }
        }
        return teacher;
    }
}
