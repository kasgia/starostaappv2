package com.starostaapp.backend.repository;

import com.starostaapp.backend.model.Group;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface GroupRepository extends MongoRepository<Group, Integer> {
    ArrayList<Group> findBySubjectname(final String subjectName);
    ArrayList<Group> findByNameofgroup(final String nameGroup);
}
