package com.starostaapp.backend.repository;

import com.starostaapp.backend.model.Teacher;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TeacherRepository extends MongoRepository<Teacher, Integer> { }
