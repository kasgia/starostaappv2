package com.starostaapp.backend.repository;

import com.starostaapp.backend.model.Exam;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface ExamRepository extends MongoRepository<Exam, Integer> {
    ArrayList<Exam> findAllBySubjectname(final String nameOfSubject);
    ArrayList<Exam> findAllByGroup(final String nameOfSubject);
    ArrayList<Exam> findByDate(final String dateOfExam);
}
