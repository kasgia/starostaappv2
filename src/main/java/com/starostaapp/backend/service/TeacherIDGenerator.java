package com.starostaapp.backend.service;

import com.starostaapp.backend.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherIDGenerator {
    private MongoOperations mongoOp;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    public TeacherIDGenerator(MongoOperations mongoOp) {
        this.mongoOp = mongoOp;
    }

    public Integer generateNewID() {
        Integer counter = 0;

        Aggregation agg = Aggregation.newAggregation(
                Aggregation.sort(Sort.Direction.DESC, "_id"),
                Aggregation.limit(1)
        );

        AggregationResults<Teacher> result = mongoTemplate.aggregate(agg, "Teacher", Teacher.class);
        List<Teacher> teacher = result.getMappedResults();
        counter = teacher.get(0).getId();

        return counter + 1;
    }
}
