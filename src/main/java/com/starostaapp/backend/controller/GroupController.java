package com.starostaapp.backend.controller;

import com.starostaapp.backend.model.Group;
import com.starostaapp.backend.model.Student;
import com.starostaapp.backend.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Component
@Controller
//@RestController
@RequestMapping("/api/v1")
public class GroupController {
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private MongoOperations mongoOperations;

    @Autowired
    private MongoTemplate mongoTemplate;

    @GetMapping("/groups")
    public String showGroupList(Model model) {
        model.addAttribute("groups", this.getAllGroups());
        return "group-list";
    }

    @GetMapping("/groups1")
    public ArrayList<Group> getAllGroups() {
        return (ArrayList<Group>) groupRepository.findAll();
    }

    @GetMapping("/groups/{nameofsubject}")
    public ArrayList<Group> getGroupsBySubjectName(@PathVariable(value = "nameofsubject") String nameOfSubject) {
        return (ArrayList<Group>) groupRepository.findBySubjectname(nameOfSubject);
    }

    @GetMapping("/groups/group/{nameOfGroup}")
    public Group getGroup(@PathVariable(value = "nameofsubject") String nameOfSubject,
                          @PathVariable(value = "nameOfGroup") String nameOfGroup) {
        ArrayList<Group> groups = (ArrayList<Group>) groupRepository.findBySubjectname(nameOfSubject);
        Group group = new Group();
        for (Group groupIter: groups) {
            if(groupIter.getNameofgroup().equals(nameOfGroup)) {
                group = groupIter;
                break;
            }
        }
        return group;
    }

    @PostMapping("/groups/student/remove")
    public String removeStudentFromGroup(@RequestParam("nameGroup") String groupName,
                                         @RequestParam("idStudent") Integer idStudent,
                                         Model model) {
        Query query = new Query().addCriteria(Criteria.where("nameofgroup").is(groupName));
        Group group = mongoOperations.findOne(query, Group.class);
        group.deleteStudent(idStudent);
        mongoOperations.save(group, "Group");
        Query queryGroup = new Query().addCriteria(Criteria.where("nameofgroup").is(groupName));
        Group groupToShow = mongoOperations.findOne(queryGroup, Group.class);
        System.out.println(groupToShow.toString());
        model.addAttribute("group_name", groupToShow.getNameofgroup());
        model.addAttribute("group_id", groupToShow.getId());
        model.addAttribute("group_students", groupToShow.getStudents());
        return "group-student-list";
    }

    @GetMapping("/group/student")
    public String showStudents(@RequestParam("nameGroup") String nameGroup,
                               Model model) {
        Query query = new Query().addCriteria(Criteria.where("nameofgroup").is(nameGroup));
        Group group = mongoOperations.findOne(query, Group.class);
        System.out.println(group.toString());
        model.addAttribute("group_name", group.getNameofgroup());
        model.addAttribute("group_id", group.getId());
        model.addAttribute("group_students", group.getStudents());
        return "group-student-list";
    }

    @PostMapping("/groups/student/new")
    public String insertToGroup(@RequestParam("nameGroup") String nameGroup,
                              @RequestParam("idStudent") Integer idStudent,
                              @RequestParam("name") String name,
                              @RequestParam("surname")String surname,
                              @RequestParam("isLeader") Boolean isLeader,
                                Model model) {
        Query query = new Query().addCriteria(Criteria.where("nameofgroup").is(nameGroup));
        Group group = mongoOperations.findOne(query, Group.class);
        group.addStudent(new Student(idStudent, name, surname, isLeader));
        mongoOperations.save(group, "Group");
        model.addAttribute("groups", this.getAllGroups());
        Query queryGroup = new Query().addCriteria(Criteria.where("nameofgroup").is(nameGroup));
        Group groupToShow = mongoOperations.findOne(queryGroup, Group.class);

        model.addAttribute("group_name", groupToShow.getNameofgroup());
        model.addAttribute("group_id", groupToShow.getId());
        model.addAttribute("group_students", groupToShow.getStudents());
        return "group-student-list";
    }

    @PostMapping("/groups/student/change")
    public String changeGroup(@RequestParam("idStudent") Integer idStudent,
                            @RequestParam("nameOldGroup") String nameOldGroup,
                            @RequestParam("nameNewGroup") String nameNewGroup,
                              Model model) {

        //Delete from one group
        Query queryOldGroup = new Query().addCriteria(Criteria.where("nameofgroup").is(nameOldGroup));
        Group oldGroup = mongoOperations.findOne(queryOldGroup, Group.class);
        Student studentToChange = oldGroup.findStudent(idStudent);
        oldGroup.deleteStudent(idStudent);
        mongoOperations.save(oldGroup, "Group");

        //insert into new group
        Query queryNewGroup = new Query().addCriteria(Criteria.where("nameofgroup").is(nameNewGroup));
        Group newGroup = mongoOperations.findOne(queryNewGroup, Group.class);
        newGroup.addStudent(studentToChange);
        mongoOperations.save(newGroup, "Group");
        model.addAttribute("group_name", oldGroup.getNameofgroup());
        model.addAttribute("group_id", oldGroup.getId());
        model.addAttribute("group_students", oldGroup.getStudents());
        return "group-student-list";
    }
}
