package com.starostaapp.backend.controller;

import com.starostaapp.backend.exception.ResourceNotFoundException;
import com.starostaapp.backend.model.Teacher;
import com.starostaapp.backend.repository.TeacherRepository;
import com.starostaapp.backend.service.TeacherIDGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sun.awt.image.ImageWatched;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class TeacherController {
    @Autowired
    private TeacherRepository teachRepository;

    @Autowired
    private TeacherIDGenerator teachIDGenerator;

    @GetMapping("/teachers")
    public ArrayList<Teacher> getAllTeacher() {
        return (ArrayList<Teacher>) teachRepository.findAll();
    }

    @GetMapping("/teachers/{id}")
    public ResponseEntity<Teacher> getTeacherById(@PathVariable(value="id") Integer teacherID) throws ResourceNotFoundException {
        Teacher teacher = teachRepository.findById(teacherID)
                            .orElseThrow(() -> new ResourceNotFoundException("Teacher not found for this id: " + teacherID.toString()));
        return ResponseEntity.ok().body(teacher);
    }

    @DeleteMapping("/teachers/{id}")
    public Map<Integer, Boolean> deleteTeacher(@PathVariable(value="id") Integer teacherID) throws  ResourceNotFoundException {
        Teacher teacher = teachRepository.findById(teacherID)
                            .orElseThrow(() -> new ResourceNotFoundException("Teacher not found for this id: " + teacherID.toString()));
        teachRepository.delete(teacher);
        Map<Integer, Boolean> response = new HashMap<>();
        response.put(0, Boolean.TRUE);
        return response;
    }
}
