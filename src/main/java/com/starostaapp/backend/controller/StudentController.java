package com.starostaapp.backend.controller;

import com.starostaapp.backend.model.Group;
import com.starostaapp.backend.model.Student;
import com.starostaapp.backend.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Controller
//@RestController
@RequestMapping("/api/v1")
public class StudentController {
    @Autowired
    private StudentRepository studentrepo;

    @Autowired
    MongoOperations mongoOperations;

    @Autowired
    GroupController gc;

    @GetMapping("/students")
    public ArrayList<Student> getAllStudents() {
        return (ArrayList<Student>) studentrepo.findAll();
    }

    @GetMapping("/")
    public String showLogWindow(){ return "index";}

    @PostMapping("/")
    public String checkPassword(@RequestParam("idStudent") Integer studentID,
                                @RequestParam("password") String password,
                                Model model){
        String page = "index";
        Query query = new Query().addCriteria(Criteria.where("id").is(studentID));
        Student student = mongoOperations.findOne(query, Student.class);
        if(student.getPassword().equals(password)) {
            page = "group-list";
            model.addAttribute("groups", gc.getAllGroups());
        } else {
            page = "incorrectLogin";
        }
        return page;
    }
}
