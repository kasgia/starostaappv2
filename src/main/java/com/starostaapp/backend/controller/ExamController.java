package com.starostaapp.backend.controller;

import com.starostaapp.backend.model.Exam;
import com.starostaapp.backend.model.Group;
import com.starostaapp.backend.model.Teacher;
import com.starostaapp.backend.repository.ExamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Controller
//@RestController
@RequestMapping("api/v1")
public class ExamController {
    @Autowired
    private ExamRepository examRepository;

    @Autowired
    private MongoOperations mongoOperations;

    static Integer ExamID = 1;

    @GetMapping("/group/exams")
    public String getAllExamsOfGroup(@RequestParam("nameGroup") String groupName,
                                     Model model) {
        ArrayList<Exam> exams =  examRepository.findAllByGroup(groupName);
        model.addAttribute("exams", exams);
        return "exam-list";
    }

    @GetMapping("/exams/subject/{nameofsubject}")
    public ArrayList<Exam> getAllExamsOfSubject(@PathVariable(value = "nameofsubject") String subject) {
        return (ArrayList<Exam>) examRepository.findAllBySubjectname(subject);
    }

    @PostMapping("/group/exams/delete")
    public String removeExam(@RequestParam("idExam") Integer idExam,
                             Model model) {
        Query query = new Query().addCriteria(Criteria.where("id").is(idExam));
        Exam examToRemove = mongoOperations.findOne(query, Exam.class);
        mongoOperations.findAndRemove(query, Exam.class);
        model.addAttribute("exams", examRepository.findAllByGroup(examToRemove.getGroup()));
        return "exam-list";
    }

    @PostMapping("/group/exams/new")
    public String addExam(@RequestParam("nameGroup") String groupName,
                        @RequestParam("date") String date,
                        Model model) {
        Query queryGroup = new Query().addCriteria(Criteria.where("nameofgroup").is(groupName));
        Group group = mongoOperations.findOne(queryGroup, Group.class);
        Teacher teacher = group.getTeachers().get(0);
        Exam examToSave = new Exam(++ExamID, group.getSubjectname(), teacher, groupName, date);
        mongoOperations.save(examToSave);
        model.addAttribute("exams", examRepository.findAllByGroup(groupName));
        return "exam-list";
    }
}
